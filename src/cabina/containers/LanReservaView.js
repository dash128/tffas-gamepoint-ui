import React,{Component, Fragment} from 'react'
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import { Button, Table, TableHead, TableRow, TableCell, TableBody, Typography ,DialogContent} from '@material-ui/core';
import API from '../../util/api'

import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';

import { blue } from '@material-ui/core/colors';
import PersonIcon from '@material-ui/icons/Person';
import DateFnsUtils from "@date-io/date-fns/"
import MuiPickersUtilsProvider from "material-ui-pickers/MuiPickersUtilsProvider"
import DatePicker from "material-ui-pickers/DatePicker"
import { da } from 'date-fns/esm/locale';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    margin: {
        margin: theme.spacing(3),
    },
    table: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
    avatar: {
        backgroundColor: blue[100],
        color: blue[600],
      },
});

class LanReservaView extends Component{
    state={
        fecha:new Date(),
        open:false,
        nombre:'',
        misCabinas:[],
        numeroPC:'',
        horarioPC:[]
    }

    handleBuscar = (nombre) => this.setState({nombre})
    CrearCabina = () =>{
        console.log('crear equipo')
        // const cabina = {
        //     numero:this.state.numeroPC,
        //     lanCenterId:1
        // }
        // API.post('Cabina',cabina)
        // .then(data => {
        //     this.ListarMisCabinas()
        // })
        // .catch(e=>console.log(e));
    }
    ListarMisCabinas = () =>{
        API.get('Cabina/ListarPorLanCenter/id?id='+1)
        .then(data => {
            console.log(data)
            this.setState({misCabinas:data.data})
        })
        .catch(e=>console.log(e));
    }

    handleChangeFecha = fecha => this.setState({fecha})
    handleChange = (numeroPC) => this.setState({numeroPC})
    handleCrearHorario =()=> {
        console.log('creare horarios')
        var count = 0;
        console.log('c',this.state.misCabinas)
        console.log('m',this.state.misCabinas.length)
        for (let i = 0; i < this.state.misCabinas.length; i++) {
            
            for (let index = 0; index < 24; index++) {
                
                const horario = {
                    fechaDia: this.state.fecha.toISOString(),
                    horaInicio:index,
                    disponibilidad:false,
                    lanCenterId:1,
                    cabinaId:this.state.misCabinas[i].id
                }
                API.post('Horario',horario)
                .then(data => {
                    count++
                    console.log(count)
                    if(count==(this.state.misCabinas.length * 24)){
                        alert('Horarios registrados')
                    }
                })
                .catch(e=>console.log(e));
                
                
            }   
        }
        
    }
    
    handleClickOpen = (id) => {
        console.log(id)
        API.get('Horario/DisponibilidadCabina/'+id+'/'+1)
        .then(data => {
            console.log(data.data)
            this.setState({horarioPC:data.data, open:true})
        })
        console.log(id)
        //this.setState({open: true})
    } 
    
    handleClickClose = () => {
        this.setState({ open: false , lanPorJuego:[]})
    }

    componentDidMount(){
        this.ListarMisCabinas()
    }
    render(){
        const { classes } = this.props;
        return(
            <Fragment>
                <br/>
                {/* <h1  style={{border:'1px solid #e8e8e8'}}>s</h1> */}

                <Paper square className={classes.root}>
                    <Grid container justify="center" spacing={1}>
                        <Grid container justify="center" spacing={1}>
                            
                            <Grid item xs={3}>
                            
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker margin="normal" label="Mostrar Horarios" value={this.state.fecha} onChange={this.handleChangeFecha}/>
                            </MuiPickersUtilsProvider>


                            </Grid>

                            <Grid item xs={2}>
                                <Button onClick={this.CrearCabina} variant="outlined" size="medium" color="primary" 
                                    className={classes.margin} onClick={this.handleCrearHorario} >
                                    Crear Horario
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid container> 
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>°N</TableCell>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.misCabinas.map(cabina => (
                                        <TableRow key={cabina.id}>
                                            <TableCell>{cabina.numero}</TableCell>
                                            <TableCell><Button onClick={_=> {this.handleClickOpen(cabina.id)}} variant="contained" size="medium" color="secondary"  >Reservas</Button></TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Paper>
                <Dialog onClose={this.handleClickClose}
                    aria-labelledby="customized-dialog-title"
                    open={this.state.open}>
                    <DialogTitle>
                        <Typography >Disponible en</Typography>
                        
                        
                    </DialogTitle>
                    <DialogContent>
                        <List>
                            {this.state.horarioPC.map( horario => (
                                <ListItem key={horario.id}>
                                    <ListItemAvatar>
                                        <Avatar className={classes.avatar}>
                                            <PersonIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText primary={(horario.disponibilidad==true) ? 'Ocupado':'Libre'} />
                                </ListItem>
                            ))}
                        </List>
                    </DialogContent>
                </Dialog>

            </Fragment>
        )
    }
}
export default withStyles(styles)(LanReservaView);