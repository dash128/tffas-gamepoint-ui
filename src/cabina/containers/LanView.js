import React,{Component, Fragment} from 'react'
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import { Button, TextField, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import API from '../../util/api'


import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    margin: {
        margin: theme.spacing(3),
    },
    table: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
});

class LanView extends Component{
    state={
        open:false,
        nombre:'',
        misCabinas:[],
        numeroPC:''
    }

    handleBuscar = (nombre) => this.setState({nombre})
    CrearCabina = () =>{
        console.log('crear equipo')
        const cabina = {
            numero:this.state.numeroPC,
            lanCenterId:1
        }
        API.post('Cabina',cabina)
        .then(data => {
            this.ListarMisCabinas()
        })
        .catch(e=>console.log(e));
    }
    ListarMisCabinas = () =>{
        API.get('Cabina/ListarPorLanCenter/id?id='+1)
        .then(data => {
            console.log(data)
            this.setState({misCabinas:data.data})
        })
        .catch(e=>console.log(e));
    }

    handleChange = (numeroPC) => this.setState({numeroPC})
    componentDidMount(){
        this.ListarMisCabinas()
    }
    render(){
        const { classes } = this.props;
        return(
            <Fragment>
                <br/>
                {/* <h1  style={{border:'1px solid #e8e8e8'}}>s</h1> */}

                <Paper square className={classes.root}>
                    <Grid container justify="center" spacing={1}>
                        <Grid container justify="center" spacing={1}>
                            
                            <Grid item xs={3}>
                            <TextField id="outlined-name"
                                label="numero PC"
                                className={classes.textField}
                                value={this.state.numeroPC}
                                onChange={e=>this.handleChange(e.target.value)}
                                margin="normal"
                                variant="outlined"/>
                            </Grid>

                            <Grid item xs={2}>
                                <Button onClick={this.CrearCabina} variant="outlined" size="medium" color="primary" className={classes.margin} onClick={this.CrearCabina} >
                                    Agregar
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid container> 
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>°N</TableCell>
                                        
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.misCabinas.map(cabina => (
                                        <TableRow key={cabina.id}>
                                            <TableCell>{cabina.numero}</TableCell>
                                            
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Paper>

            </Fragment>
        )
    }
}
export default withStyles(styles)(LanView);