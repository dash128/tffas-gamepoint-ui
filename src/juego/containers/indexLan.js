import React, { Component } from 'react'
import {withStyles} from '@material-ui/core'
import {Table, TableBody, TableCell, TableHead, TableRow, Paper} from '@material-ui/core/'
import {Switch} from '@material-ui/core/'
const styles = theme => ({
  root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      marginLeft: theme.spacing.unit * 6,
      marginRigth: theme.spacing.unit * 6,
      overflowX: 'auto',
  },
  table: {
      minWidth: 650,
  },
})  

class Juegos extends Component {
  state={
    juegos:[{
      name:'Dota 2',
      estado:true
    },{
      name:'Lol',
      estado:false
    },{
      name:'Conter',
      estado:false
    },{
      name:'Dota 1',
      estado:false
    }]
  }
  handleChange = name => event => {
    // console.log(event.target.checked);
    var aux = this.state.juegos
    // aux[]
    this.setState({[name]: event.target.checked })
    // setState({ ...state, [name]: event.target.checked });
  };
  render() {
    const {classes} = this.props
    return (
      <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Juego </TableCell>
            <TableCell align="right"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.state.juegos.map(row => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">
              <Switch
                checked={row.estado}
                onChange={this.handleChange('estado')}
                value="estado"
                inputProps={{ 'aria-label': 'secondary checkbox' }}/>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    )
  }
}

export default withStyles(styles)(Juegos)
