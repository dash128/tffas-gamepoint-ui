import React, {Fragment, Component } from 'react'
import API from '../../util/api'
import {withStyles, Grid, Paper, DialogContent} from '@material-ui/core'

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';


import { blue } from '@material-ui/core/colors';
import PersonIcon from '@material-ui/icons/Person';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const styles = theme => ({
  root: {
      width: '100%',
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(6),
      marginRigth: theme.spacing(6),
      overflowX: 'auto',
  },
  table: {
      minWidth: 650,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
})  

class Juegos extends Component {
    state={
        open:false,
        juegos:[],
        lanPorJuego:[]
    }
    handleChange = name => event => {

        var aux = this.state.juegos
        
        this.setState({[name]: event.target.checked })
    
    };
    handleClickOpen = (id) => {
        API.get('JuegoPorLanCenter/BuscarLanCenterPorJuego/'+id)
        .then(data => {
            this.setState({lanPorJuego:data.data})
        })
        console.log(id)
        this.setState({open: true})
    } 
    
    handleClickClose = () => {
        this.setState({ open: false , lanPorJuego:[]})
    }
 

    componentDidMount(){
        this.mounted = true;

        API.get('juego')
        .then(data => {
            this.setState({juegos:data.data})
        })
        .catch(e=>console.log(e));
    }
    componentWillUnmount(){
        this.mounted = false;
    }
    render() {
        const {classes} = this.props
        return (
        <Fragment>

        
        <Paper className={classes.root}>
            <Grid container justify="flex-start" spacing={4}>
                {this.state.juegos.map(juego => (
                    <Grid item key={juego.id} xs={3}>
                        <Card className={classes.card}>
                            <CardActionArea>
                            <CardMedia
                                component="img"
                                alt="Contemplative Reptile"
                                height="240"
                                image={juego.url}
                                title="Contemplative Reptile"/>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {juego.nombre}
                                </Typography>
                                
                            </CardContent>
                            </CardActionArea>
                            <CardActions>
                            <Button size="small" color="primary">
                                Agregar
                            </Button>
                            <Button size="small" color="primary" onClick={_=> {this.handleClickOpen(juego.id)
                                                                                console.log(1)}}>
                                En LanCenters
                            </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        </Paper>
            <Dialog onClose={this.handleClickClose}
                aria-labelledby="customized-dialog-title"
                open={this.state.open}>
                <DialogTitle>
                    <Typography >Disponible en</Typography>
                    
                        {/* <IconButton aria-label="Close" className={classes.closeButton} onClick={this.handleClickClose}>
                         <CloseIcon />
                        </IconButton> */}
                    
                </DialogTitle>
                <DialogContent>
                    <List>
                        {this.state.lanPorJuego.map( lancenter => (
                            <ListItem key={lancenter.lanCenterId}>
                                <ListItemAvatar>
                                    <Avatar className={classes.avatar}>
                                        <PersonIcon />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={lancenter.lanCenter.nombre} />
                            </ListItem>
                        ))}
                    </List>
                </DialogContent>
            </Dialog>
        </Fragment>
        )
    }
}

export default withStyles(styles)(Juegos)