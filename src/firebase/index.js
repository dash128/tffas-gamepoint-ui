import * as firebase from './config'
import * as fs from './firestore'
import * as auth from './auth'

export {
    firebase,
    fs,
    auth
}