import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyDSsKxzLyN_qyJb6hCRMgJSE_IHoJO8nK0",
    authDomain: "clan-battle-eba56.firebaseapp.com",
    databaseURL: "https://clan-battle-eba56.firebaseio.com",
    projectId: "clan-battle-eba56",
    storageBucket: "clan-battle-eba56.appspot.com",
    messagingSenderId: "842247940272",
    appId: "1:842247940272:web:e9a042065b209d0b"
  };

  if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig)
}

const fs = firebase.firestore()
const auth = firebase.auth()

export {
    auth,
    fs,
    firebase
}