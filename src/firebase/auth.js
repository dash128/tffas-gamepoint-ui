import {auth, firebase} from './config'

export const SignInWithGoogle = () => {
    // console.log('google')
    // const provider =  firebase.auth.GoogleAuthProvider()

    // const provider =  auth.GoogleAuthProvider()

    // auth.signInWithPopup(provider)
    // console.log('google')
    // .then( result=>{
    //     console.log('Hola x: '+ result.user.displayName)
    // })
    // .catch( error => console.error( 'Error: '+ error.code + ' : '+ error.message) )

    const provider = new firebase.auth.GoogleAuthProvider()
    
    auth.signInWithPopup(provider)
    .then( result => {
        CrearUserEnFirebase(result.user.providerData[0])
        console.log('Hola x: '+ result.user.displayName)
        
    })
    .catch( error => console.error( 'Error: '+ error.code + ' : '+ error.message) )
    
}

export const SignInWithEmail = (email, pass) => {

    auth.signInWithEmailAndPassword(email, pass)
    .then( (user) => console.log('Te has conectado', user) )
    .catch( error => console.error( 'Error: '+ error.code + ' : '+ error.message) )
    // console.log(this.state.password)
}

export const CreateUserWithEmail = (email, pass) => {

}

export const LogOut = () =>{
    auth.signOut()
    .then( () => console.log('Te has desconectado') )
    .catch( error => console.error( 'Error: '+ error.code + ' : '+ error.message) )
}