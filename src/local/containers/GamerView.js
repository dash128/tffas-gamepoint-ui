import React,{Component, Fragment} from 'react'
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import Tab1 from './Tab1'
import Tab2 from './Tab2'
import Tab3 from './Tab3'

const styles = theme => ({
    root: {
        flexGrow: 1,
        // maxWidth: 500,
    },
});

class GamerView extends Component{
    state={
        value:0
    }

    handleChangeTab = (event, value) =>this.setState({value})
    render(){
        const { classes } = this.props;
        return(
            <Fragment>
                <Paper square className={classes.root}>
                    <Tabs style={{border:'1px solid #e8e8e8'}}
                        value={this.state.value}
                        onChange={this.handleChangeTab}
                        variant="fullWidth"
                        indicatorColor="secondary"
                        textColor="secondary">
                        <Tab icon={<PhoneIcon />} label="nombre" />
                        <Tab icon={<PersonPinIcon />} label="lugar" />
                        <Tab icon={<FavoriteIcon/>} label="VIP" />
                    </Tabs> 
                </Paper>
                
                {this.state.value === 0 && <Tab1/>}
                {this.state.value === 1 && <Tab2/>}
                {this.state.value === 2 && <Tab3/>}
            </Fragment>
        )
    }
}
export default withStyles(styles)(GamerView);