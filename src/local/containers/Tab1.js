import React,{Component, Fragment} from 'react'
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import { Button, TextField, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import API from '../../util/api'


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    margin: {
        margin: theme.spacing(3),
    },
    table: {
        width: '100%',
        marginTop: theme.spacing(3),
        overflowX: 'auto',
    },
});

class Tab1 extends Component{
    state={
        nombre:'',
        locales:[],
    }

    handleBuscar = (nombre) => this.setState({nombre})
    BuscarFromApi = () =>{
        console.log('click')
        API.get('LanCenter/BuscarConNombre/'+this.state.nombre)
        .then(data => {
            console.log(data.data)
            this.setState({locales:data.data})
        })
        .catch(e=>console.log(e));
    }
    render(){
        const { classes } = this.props;
        return(
            <Fragment>
                <br/>
                {/* <h1  style={{border:'1px solid #e8e8e8'}}>s</h1> */}

                <Paper square className={classes.root}>
                    <Grid container justify="center" spacing={1}>
                        <Grid container justify="center" spacing={1}>
                            <Grid item xs={3}>
                            <TextField id="outlined-name"
                                label="Nombre"
                                className={classes.textField}
                                value={this.state.nombre}
                                onChange={e=>this.handleBuscar(e.target.value)}
                                margin="normal"
                                variant="outlined"/>
                            </Grid>
                            <Grid item xs={2}>
                                <Button variant="outlined" size="medium" color="primary" className={classes.margin} onClick={this.BuscarFromApi} >
                                    Buscar
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid container> 
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Nombre</TableCell>
                                        <TableCell>Correo</TableCell>
                                        <TableCell>Distrito</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.state.locales.map(local => (
                                        <TableRow key={local.id}>
                                            <TableCell>{local.nombre}</TableCell>
                                            <TableCell>{local.correo}</TableCell>
                                            <TableCell>{local.distrito}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Grid>
                    </Grid>
                </Paper>
            </Fragment>
        )
    }
}
export default withStyles(styles)(Tab1);