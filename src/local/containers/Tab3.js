import React,{Component, Fragment} from 'react'
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';





const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: 500,
    },
});

class Tab3 extends Component{
    state={
        value:0
    }

    handleChangeTab = (event, value) =>this.setState({value})
    render(){
        const { classes } = this.props;
        return(
            <Fragment>
                <Paper square className={classes.root}>
                    3
                </Paper>
            </Fragment>
        )
    }
}
export default withStyles(styles)(Tab3);