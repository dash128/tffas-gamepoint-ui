import React, { Component, Fragment } from 'react'

import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';

import Icon from '@material-ui/core/Icon';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';

import Lista from '../components/Lista'

class Torneos extends Component {
  state={
    value:'recents'
  }
  handleChange = (event, value) =>this.setState({value})
  render() {
    return (
      <Fragment>
        <BottomNavigation value={this.state.value} onChange={this.handleChange} >
          <BottomNavigationAction label="Recents" value="recents" icon={<RestoreIcon />} />
          <BottomNavigationAction label="Favorites" value="favorites" icon={<FavoriteIcon />} />
          <BottomNavigationAction label="Nearby" value="nearby" icon={<LocationOnIcon />} />
          <BottomNavigationAction label="Folder" value="folder" icon={<Icon>folder</Icon>} />
        </BottomNavigation>
        {this.state.value === "recents" && <Lista/>}
        {this.state.value === "favorites" && <h1>Item rwo</h1>}
        {this.state.value === "nearby" && <h1>Item tree</h1>}
        {this.state.value === "folder" && <h1>Item One</h1>}
      </Fragment>
    )
  }
}

export default Torneos
