import React from 'react'
import {withStyles} from '@material-ui/core'
// import { makeStyles } from '@material-ui/core/styles';
import {Table, TableBody, TableCell, TableHead, TableRow, Paper} from '@material-ui/core/'
import Button from '@material-ui/core/Button';
const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 650,
    },
})  

  function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
  }
  
  const rows = [
    createData('TI 6 Monterrico', '2019-01-05', 8, 'Dota', 4.0),
    createData('TI 5 Monterrico', '2018-01-05', 12,'Dota', 4.3),
    createData('TI 4 Monterrico', '2017-01-05', 16, 'Dota', 6.0),
    createData('TI 3 Monterrico', '2016-01-05', 8, 'Dota', 4.3),
    createData('TI 2 Monterrico', '2015-01-05', 4, 'Dota', 3.9),
  ];

const ListaTorneo = (props) => {
    const {classes} = props

    return (
        <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Nombre </TableCell>
            <TableCell align="right">Fecha</TableCell>
            <TableCell align="right">Cantidad</TableCell>
            <TableCell align="right">Juego</TableCell>
            <TableCell align="right"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.calories}</TableCell>
              <TableCell align="right">{row.fat}</TableCell>
              <TableCell align="right">{row.carbs}</TableCell>
              <TableCell align="right"><Button variant="contained" color="primary">Integrantes</Button></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
    )
}

export default withStyles(styles)(ListaTorneo)
