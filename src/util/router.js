const routerPageSus = 'suscripcion'
const routerPageHome = 'home'

const routerLanCabina = 'cabinas'
const routerLanTorneo = 'torneo'
const routerLanReserva = 'reserva'
const routerLanPerfil = 'perfil'

const routerGameLocal = 'lancenter'
const routerGameJuego = 'juego'
const routerGameReto = 'versus'
const routerGameTorneo = 'torneo'
const routerGameEquipo = 'equipo'
const routerGameReserva = 'reserva'
const routerGamePerfil = 'perfil'


export {
    routerPageSus,
    routerPageHome,
    
    routerLanCabina,
    routerLanTorneo,
    routerLanReserva,
    routerLanPerfil,

    routerGameLocal,
    routerGameJuego,
    routerGameReto,
    routerGameTorneo,
    routerGameEquipo,
    routerGameReserva,
    routerGamePerfil

}