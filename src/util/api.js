import axios from 'axios';

export default axios.create({
    baseURL: 'https://gamepoint-apirest.azurewebsites.net/api/'
});