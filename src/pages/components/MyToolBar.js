import React, {Fragment} from 'react'
import {auth} from '../../firebase'
import clsx from 'clsx';
import {Link} from 'react-router-dom'

import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar'
import ToolBar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {routerPageHome, routerPageSus} from '../../util/router'

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    appBar2: {
        position: 'relative',
    },
    toolbarTitle: {
        flex: 1,
    },
}));


const MyToolBar = (props) => {
    const classes = useStyles();

    return ( 
        <Fragment>
        {
            (props.user) ? (
            <AppBar position="fixed" className={clsx(classes.appBar, {
                [classes.appBarShift]: props.open,
            })}>
                <ToolBar>
                <IconButton color="inherit" aria-label="Open drawer"
                    onClick={props.handleDrawerOpen} edge="start"
                    className={clsx(classes.menuButton, {
                     [classes.hide]: props.open,
                })}>
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" noWrap>
                    Dash Tenynnson
                </Typography>
                </ToolBar>
            </AppBar>
            )
            :
            (
            <AppBar position='static' color="default" className={classes.appBar2}>
                <ToolBar>
                    <Typography variant='h5' color='inherit' noWrap className={classes.toolbarTitle}>Clan Battle</Typography>
                    
                    <Button component={Link} to={routerPageHome}>Home</Button>
                    <Button component={Link} to={routerPageSus}>Suscripcion</Button>
                    <Button onClick={auth.SignInWithGoogle} color='primary' variant='contained'>
                        Login
                    </Button>
                </ToolBar>
            </AppBar>
            )
        }
        </Fragment>
    );
}
 
export default (MyToolBar);