import React from 'react'
import {Link} from 'react-router-dom'

import withStyles from '@material-ui/core/styles/withStyles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'

const styles = theme => ({
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        [theme.breakpoints.up(900 + theme.spacing(6))]: {
          width: 900,
          marginLeft: 'auto',
          marginRight: 'auto',
        },
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing(8)}px 0 ${theme.spacing(6)}px`,
    },
    cardGrid: {
        padding: `${theme.spacing(8)}px 0`,
    },    
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
})

const MainIndex = (props) => {
    const { classes } = props;

    return ( 
        <main className={classes.layout}>
            <div className={classes.heroContent}>
                <div>
                    <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                        Clan Battle
                    </Typography>
                    <Typography variant="h6" align="center" color="textSecondary" paragraph>
                        Bienvenidos a la comunidad que une a todos los gamers peruanos con el fin de formar vinculos entre todo el ecosistema. 
                        Disfruta la experiencia y crecé como gamer profesional en este mundo echo para ti.
                    </Typography>
                    <div>
                        <Grid container spacing={1} justify="center">
                            <Grid item>

                                <Button variant="contained" color="primary">
                                    Gamer
                                </Button>
                                
                            </Grid>
                            <Grid item>
                                <Button variant="outlined" color="primary">
                                    <a href='/LanCenter.html'>Lan Center</a>
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </div>
            <Grid container spacing={4} className={classes.cardGrid}>
                    {props.juegos.map(juego=>(
                        <Grid item key={juego.id} sm={6} md={4} lg={3}>
                            <Card className={classes.card}>
                                <CardMedia 
                                    className={classes.cardMedia}
                                    image={juego.url}
                                    title={juego.nombre}/>
                                <CardContent className={classes.cardContent}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {juego.nombre}
                                    </Typography>
                                    
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                    
                </Grid>

        </main>
    );
}
 
export default withStyles(styles)(MainIndex);