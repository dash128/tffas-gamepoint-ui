import React from 'react'

import withStyles from '@material-ui/core/styles/withStyles'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'

const styles = theme => ({
    '@global': {
      body: {
        backgroundColor: theme.palette.common.white,
      },
    },
    appBar: {
      position: 'relative',
    },
    toolbarTitle: {
      flex: 1,
    },
    layout: {
      width: 'auto',
      marginLeft: theme.spacing(3),
      marginRight: theme.spacing(3),
      [theme.breakpoints.up(900 + theme.spacing(6))]: {
        width: 900,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
    heroContent: {
      maxWidth: 600,
      margin: '0 auto',
      padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing(6)}px`,
    },
    cardHeader: {
      backgroundColor: theme.palette.grey[200],
    },
    cardPricing: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'baseline',
      marginBottom: theme.spacing(2),
    },
    cardActions: {
      [theme.breakpoints.up('sm')]: {
        paddingBottom: theme.spacing(2),
      },
    }
  });

const planesSubcripcion = [
    {
        titulo:'Free',
        precio:'0',
        buttonTexto:'Empieza ya',
        buttonStyle:'outlined'
    },{
        titulo:'Divine',
        cabecera:'Popular',
        precio:'15',
        buttonTexto:'Registrate',
        buttonStyle:'contained'
    },{
        titulo:'Arconte',
        precio:'5',
        buttonTexto:'Contactanos',
        buttonStyle:'outlined'
    }
]

const Suscripcion = (props) => {
    const { classes } = props;

    return (  
        <main className={classes.layout}>
            <div className={classes.heroContent}>
                <div>
                    <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                        Suscribete
                    </Typography>
                    <Typography variant="h6" align="center" color="textSecondary" paragraph>
                        Si perteneces a una lan center y quieres ser parte de la comunidad, suscribete y se parte de muchos beneficios y suamte a nuestra comunidad
                    </Typography>
                </div>
                <Grid container spacing={4} alignItems="flex-end">
                    {props.planes.map(plan=>(
                        <Grid item key={plan.id} xs={12} sm={(plan.id === 2) ? 12 : 6} md={4}>
                            <Card>
                                <CardHeader 
                                    title={plan.nombre}
                                    subheader={(plan.id === 2) ? 'Unete' : null}
                                    titleTypographyProps={{ align: 'center' }}
                                    subheaderTypographyProps={{ align: 'center' }}
                                    className={classes.cardHeader}/>
                                <CardContent>
                                    <div className={classes.cardPricing}>
                                        <Typography component="h2" variant="h3" color="textPrimary">
                                            ${plan.precio}
                                        </Typography>
                                        <Typography variant="h6" color="textSecondary">
                                            /dolares
                                        </Typography>
                                    </div>
                                </CardContent>
                                <CardActions className={classes.cardActions}>
                                    <Button fullWidth variant={'outlined'} color="primary">
                                        Empieza Ya
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </div>
        </main>
    );
}
 
export default withStyles(styles)(Suscripcion);