import React, { Component, Fragment } from 'react';
import {Switch, Route} from 'react-router-dom'
import {auth, firebase} from '../../firebase'
import Login from '../../_lancenter/components/login'
import MainLan from '../../_lancenter/containers/index'



class LanCenter extends Component {
    state={
        user:null
    }

    componentWillMount(){
        firebase.auth.onAuthStateChanged(user=>{
            // user ? console.log('IF') : console.log('ELSE')
            user ? this.setState({user:1}) : this.setState({user:null})

        })
    }

    render() {
        return (
            <Fragment>
                {(!this.state.user) ? 
                    (<Login user={this.state.user}/>)
                    :
                    (<MainLan user={this.state.user} />)}
            </Fragment>
        )
    }
}
export default LanCenter;
