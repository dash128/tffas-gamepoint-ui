import React, { Component, Fragment } from 'react'
import {firebase} from '../../firebase'
import {Switch, Route} from 'react-router-dom'
import {routerGameLocal, routerGameJuego, routerGameReto, routerGameTorneo, routerGameEquipo, routerGameReserva, routerGamePerfil} from '../../util/router'

import CssBaseline from '@material-ui/core/CssBaseline'
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import withStyles from '@material-ui/core/styles/withStyles'
import blue from '@material-ui/core/colors/blue'
import teal from '@material-ui/core/colors/teal'

import MyToolBar from '../components/MyToolBar'
import MyDrawer from '../../_gamer/components/MyDrawer'
import PAGE from './Index'
import LocalView from '../../local/containers/GamerView'
import JuegoView from '../../juego/containers/GameView'
import EquipoView from '../../equipo/containers/GamerView'

const theme = createMuiTheme({
    palette:{
        primary:blue,
        secondary:teal
    },
    typography: {
        useNextVariants: true
    }
})

const styles = theme => ({
    root: {
      display: 'flex',
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: '0 8px',
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  });

class App extends Component {
    state = { 
        user:null,
        open:false
    }
    handleDrawerOpen = () => this.setState({ open: true })
    handleDrawerClose = () => this.setState({ open: false })
    componentWillMount(){
        firebase.auth.onAuthStateChanged(authUser=>{
            authUser ? console.log('ifM') : console.log('ElseM')
            // authUser ? (this.setState({user: authUser.providerData[0]})) : (this.setState({user:null}))
            authUser ? this.setState({user:1}) : this.setState({user:null})
        })
     }
    render() { 
        const { classes } = this.props;
        return (  
            <Fragment>
                <CssBaseline/>
                <MuiThemeProvider theme={theme}>
                    {(!this.state.user) ? 
                        (<PAGE/>)
                        :
                        (<div className={classes.root}>
                            <MyToolBar user={this.state.user} open={this.state.open} handleDrawerOpen={this.handleDrawerOpen} handleDrawerClose={this.handleDrawerClose}/>
                            <MyDrawer user={this.state.user} open={this.state.open} handleDrawerClose={this.handleDrawerClose}/>
                            <main className={classes.content}>
                                <div className={classes.toolbar} />
                                <Switch>
                                    {/* <Route  path={'/'+routerGameLocal} component={Gamer} /> */}
                                    <Route  path={'/'+routerGameLocal} render={()=><LocalView/>} />
                                    <Route  path={'/'+routerGameJuego} render={()=><JuegoView/>} />
                                    <Route  path={'/'+routerGameReto} render={()=><h1>Retos</h1>} />
                                    <Route  path={'/'+routerGameTorneo} render={()=><h1>Torneos</h1>} />
                                    <Route  path={'/'+routerGameEquipo} render={()=><EquipoView/>} />
                                    <Route  path={'/'+routerGameReserva} render={()=><h1>REserva</h1>} />
                                    <Route  path={'/'+routerGamePerfil} render={()=><h1>perfil</h1>} />
                                    <Route  path={'/'} render={()=><LocalView/>} />

                                </Switch>
                            </main>
                        </div>)}

                </MuiThemeProvider>
                
            </Fragment>
        );
    }
}
 
export default withStyles(styles)(App);