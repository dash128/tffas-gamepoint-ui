import React, { Component, Fragment } from 'react'
import {Switch, Route} from 'react-router-dom'
import {routerPageHome, routerPageSus} from '../../util/router'
import API from '../../util/api'

import MyToolBar from '../components/MyToolBar'
import MyFooter from '../components/MyFooter'
import Main from '../components/Main'
import Suscripcion from '../components/Suscripcion'

class Index extends Component {
    state = { 
        juegos:[],
        planes:[]
     }

    componentWillMount(){
        this.mounted = true;

        API.get('juego')
        .then(data => {
            this.setState({juegos:data.data})
        })
        .catch(e=>console.log(e));

        API.get('suscripcion')
        .then(data => {
            this.setState({planes:data.data})
        })
        .catch(e=>console.log(e));
    }
    componentWillUnmount(){
        this.mounted = false;
    }

    render() { 
        return (  
            <Fragment>
                <MyToolBar match={this.props.match} />
                <Switch>
                    <Route exact path={`/`} render={()=><Main juegos={this.state.juegos}/>} />
                    <Route exact path={`/`+routerPageSus} render={()=> <Suscripcion planes={this.state.planes}/>} />
                    {/* <Route exact path={`/`+routerPageHome} component={Main} /> */}
                    <Route exact path={`/`+routerPageHome} render={()=><Main juegos={this.state.juegos}/>} />
                </Switch>
                <MyFooter />
            </Fragment>
            
        );
    }
}
 
export default Index;