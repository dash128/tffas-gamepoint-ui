import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter} from 'react-router-dom'

import Lan from '../pages/containers/LanCenter'

let lan = document.getElementById('lan')
let Main = <BrowserRouter> <Lan/> </BrowserRouter> 

ReactDOM.render(Main,lan)
