import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter} from 'react-router-dom'

import App from '../pages/containers/App'

let app = document.getElementById('app')
let Main = <BrowserRouter> <App/> </BrowserRouter> 

ReactDOM.render(Main,app)
