import React from 'react'
import {Link} from 'react-router-dom'

import withStyles from '@material-ui/core/styles/withStyles'
import AppBar from '@material-ui/core/AppBar'
import ToolBar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    toolbarTitle: {
        flex: 1,
    },
})

const MyToolBar = (props) => {
    const { classes } = props;

    return ( 
        <AppBar position='static' className={classes.appBar}>
            <ToolBar>
                <Typography variant='h5' color='inherit' noWrap className={classes.toolbarTitle}>My Name</Typography>
                {/* <Button component={Link} to={`${props.match.url}`}>Home</Button>
                <Button component={Link} to={`${props.match.url}suscripcion`}>Suscripcion</Button>
                <Button component={Link} to={`${props.match.url}login`} color='primary' variant='contained'> */}
                <Button component={Link} to={`/gamer/`}>Main</Button>
                <Button component={Link} to={`/gamer/lancenter`}>LanCenter</Button>
                <Button component={Link} to={`/gamer/team`}>Team</Button>

            </ToolBar>
        </AppBar>
    );
}
 
export default withStyles(styles)(MyToolBar);