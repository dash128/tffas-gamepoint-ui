import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AccountIcon from '@material-ui/icons/AccountCircleRounded'
import LogoutIcon from '@material-ui/icons/PowerSettingsNewRounded'
import GameIcon from '@material-ui/icons/WhatshotRounded'
import VersusIcon from '@material-ui/icons/RouterRounded'
import LocalIcon from '@material-ui/icons/LocalPlayRounded'
import EquipoIcon from '@material-ui/icons/GroupRounded'
import CalendarIcon from '@material-ui/icons/EventNote'
import LanCenterIcon from '@material-ui/icons/StoreMallDirectoryRounded'
import {Link} from 'react-router-dom'
import {auth} from '../../firebase'
import {routerGameLocal, routerGameJuego, routerGameReto, routerGameTorneo, routerGameEquipo, routerGameReserva, routerGamePerfil} from '../../util/router'
const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
}));

export default function MyDrawer(props) {
  const classes = useStyles();
  const theme = useTheme();


return (
  <Drawer open={props.open}
    variant="permanent"
    className={clsx(classes.drawer, {
     [classes.drawerOpen]: props.open,
     [classes.drawerClose]: !props.open,
    })}
    classes={{
      paper: clsx({
        [classes.drawerOpen]: props.open,
        [classes.drawerClose]: !props.open,
      }),}}>
    <div className={classes.toolbar}>
      <IconButton onClick={props.handleDrawerClose}>
        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
      </IconButton>
    </div>
    <Divider />
    <List>
        <ListItem button component={Link} to={routerGameLocal} >
            <ListItemIcon>
                <LanCenterIcon /> 
            </ListItemIcon>
            <ListItemText primary="LanCenter" />
        </ListItem>
        <ListItem button component={Link} to={routerGameJuego} >
            <ListItemIcon>
            <GameIcon /> 
            </ListItemIcon>
            <ListItemText primary="Juego" />
        </ListItem>
        <ListItem button component={Link} to={routerGameReto}>
            <ListItemIcon>
                <VersusIcon /> 
            </ListItemIcon>
            <ListItemText primary="Versus" />
        </ListItem>
        <ListItem button component={Link} to={routerGameTorneo}>
            <ListItemIcon>
                <LocalIcon /> 
            </ListItemIcon>
            <ListItemText primary="Torneo" />
        </ListItem>
        <ListItem button component={Link} to={routerGameEquipo}>
            <ListItemIcon>
                <EquipoIcon /> 
            </ListItemIcon>
            <ListItemText primary="Equipos" />
        </ListItem>
        <ListItem button component={Link} to={routerGameReserva}>
            <ListItemIcon>
                <CalendarIcon /> 
            </ListItemIcon>
            <ListItemText primary="Reserva" />
        </ListItem>
        </List>
        <Divider />
        <List>
        <ListItem button component={Link} to={routerGamePerfil}>
            <ListItemIcon>
                <AccountIcon /> 
            </ListItemIcon>
            <ListItemText primary="Mi perfil" />
        </ListItem>
        <ListItem button onClick={_=> auth.LogOut}>
            <ListItemIcon>
                <LogoutIcon /> 
            </ListItemIcon>
            <ListItemText primary="Salir" />
        </ListItem>
    </List>
  </Drawer>

  );
}