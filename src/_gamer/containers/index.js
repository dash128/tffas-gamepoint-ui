import React, { Component, Fragment } from 'react'
import {Switch, Route} from 'react-router-dom'

import MyToolBar from '../components/MyToolBar'

class IndexGamer extends Component {
    state = {  }
    render() { 
        return ( 
            <Fragment>
                <MyToolBar/>
                <h1>En el Gamer</h1>
                <Switch>
                    <Route  path='/gamer/findLanCenterAll' component={Gamer} />
                    <Route  path='/gamer/team' component={LanCenter} />
                    <Route  path='/gamer/' component={Index} />
                </Switch>
            </Fragment>
        );
    }
}
 
export default IndexGamer;