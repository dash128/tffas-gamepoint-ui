import React,{ Fragment, Component} from 'react';
import clsx from 'clsx';
import {Switch, Route} from 'react-router-dom'
import {routerLanCabina, routerLanTorneo, routerLanReserva, routerLanPerfil} from '../../util/router'

import { makeStyles, withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { pink, red, blue, purple, amber} from '@material-ui/core/colors'

import MyToolBar from '../components/MyToolBar'
import MyDrawer from '../components/MyDrawer'
import CabinaView from '../../cabina/containers/LanView'
import ReservaView from '../../cabina/containers/LanReservaView'

const drawerWidth = 240;
const theme = createMuiTheme({
	palette:{
		primary: pink,
		secondary: amber,
		error: red,
	},
	typography:{
		 useNextVariants: true,
	}
})
const styles = theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
});


class MenuLanCenter extends Component {
  // const theme = useTheme();
  
  state={
    open:true
  }

  handleDrawerOpen = () => this.setState({ open: true })
  handleDrawerClose = () => this.setState({ open: false })


  render(){
    const { classes } = this.props;
    const root = '/despliegueFAS'
    return(
      <Fragment >
        <CssBaseline />
        <MuiThemeProvider theme={theme}>
          <div className={classes.root}>
            <MyToolBar user={this.state.user} open={this.state.open} handleDrawerOpen={this.handleDrawerOpen} handleDrawerClose={this.handleDrawerClose}/>
            <MyDrawer user={this.state.user} open={this.state.open} handleDrawerClose={this.handleDrawerClose}/>
            
            <main className={classes.content}>
              <div className={classes.toolbar} />
              <Switch>
                
                <Route  path={root+'/'+routerLanCabina} render={()=><CabinaView/>} />
                <Route  path={root+'/'+routerLanTorneo} render={()=><h1>torneo</h1>} />
                <Route  path={root+'/'+routerLanReserva} render={()=><ReservaView/>} />
                <Route  path={root+'/'+routerLanPerfil} render={()=><h1>Torneos</h1>} />
          
                <Route  path={root+'/'} render={()=><ReservaView/>} />

                </Switch>
            </main>
          </div>
        </MuiThemeProvider>
      </Fragment>
    );
  }

}
export default withStyles(styles)(MenuLanCenter)