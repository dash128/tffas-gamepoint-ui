import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AccountIcon from '@material-ui/icons/AccountCircleRounded'
import LogoutIcon from '@material-ui/icons/PowerSettingsNewRounded'
import DesktopIcon from '@material-ui/icons/DesktopWindowsRounded'
import LocalIcon from '@material-ui/icons/LocalPlayRounded'
import CalendarIcon from '@material-ui/icons/EventNote'
import {Link} from 'react-router-dom'
import {auth} from '../../firebase'
import {routerLanCabina, routerLanTorneo, routerLanReserva, routerLanPerfil} from '../../util/router'
const drawerWidth = 240;
const root = '/despliegueFAS'
const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
}));

export default function MyDrawer(props) {
  const classes = useStyles();
  const theme = useTheme();


return (
  <Drawer open={props.open}
    variant="permanent"
    className={clsx(classes.drawer, {
     [classes.drawerOpen]: props.open,
     [classes.drawerClose]: !props.open,
    })}
    classes={{
      paper: clsx({
        [classes.drawerOpen]: props.open,
        [classes.drawerClose]: !props.open,
      }),}}>
    <div className={classes.toolbar}>
      <IconButton onClick={props.handleDrawerClose}>
        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
      </IconButton>
    </div>
    <Divider />
    <List>
      <ListItem button component={Link} to={root+'/'+routerLanCabina+''} >
        <ListItemIcon>
          <DesktopIcon /> 
        </ListItemIcon>
        <ListItemText primary="Cabinas" />
      </ListItem>
      <ListItem button component={Link} to={root+'/'+routerLanTorneo+''}>
        <ListItemIcon>
          <LocalIcon /> 
        </ListItemIcon>
        <ListItemText primary="Torneos" />
      </ListItem>
      <ListItem button component={Link} to={root+'/'+routerLanReserva+''}>
        <ListItemIcon>
          <CalendarIcon /> 
        </ListItemIcon>
        <ListItemText primary="Reservas" />
      </ListItem>
    </List>
    <Divider />
    <List>
      <ListItem button component={Link} to={root+'/'+routerLanPerfil+''}>
        <ListItemIcon>
          <AccountIcon /> 
        </ListItemIcon>
        <ListItemText primary="Mi perfil" />
      </ListItem>
      <ListItem button onClick={_=> auth.LogOut()}>
        <ListItemIcon>
          <LogoutIcon /> 
        </ListItemIcon>
        <ListItemText primary="Salir" />
      </ListItem>
    </List>
  </Drawer>

  );
}