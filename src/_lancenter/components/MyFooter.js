import React from 'react';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

const MyFooter = () => {

    return(
        <Typography variant="body2" color="textSecondary" align="center">
            {'Built with love by '}
            <Link color="inherit" href="https://www.facebook.com/jordy.rojasaliaga">
                Team GamePoint (M++)
            </Link>
            {' team.'}
        </Typography>
    )
}
export default MyFooter