import React, {Component} from 'react'
import {withStyles } from '@material-ui/core/styles';
import {auth} from '../../firebase'

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';

import MadeWithLove from '../components/MyFooter'

  const styles = theme => ({
    root: {
      height: '100vh',
    },
    image: {
    //   backgroundImage: 'url(https://source.unsplash.com/random)',
      backgroundImage: 'url(https://i.pinimg.com/originals/e4/11/57/e41157da9a3106154632a6779f2f0add.jpg)',
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  });

  class LoginLanCenter extends Component{
    state={
      email:'',
      password:''
    }
    onChangeDatos = name => event => {
      this.setState({[name]: event.target.value})
    }
    ValidarCuenta = () => {
      auth.SignInWithEmail(this.state.email, this.state.password)
    }
    render(){
      const { classes } = this.props;

      return(
        <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <div className={classes.form} noValidate >
            <TextField id="email"
              variant="outlined"
              margin="normal"
              required fullWidth autoFocus
              label="Email Address"
              name="email"
              value={this.state.email}
              onChange={this.onChangeDatos('email')}
              autoComplete="email"/>
            <TextField id="password"
              variant="outlined"
              margin="normal"
              required fullWidth
              name="password"
              label="Password"
              type="password" 
              value={this.state.password}
              onChange={this.onChangeDatos('password')}
              autoComplete="current-password"/>
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"/>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              onClick={this.ValidarCuenta}
              className={classes.submit}>
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <MadeWithLove />
            </Box>
          </div>
        </div>
      </Grid>
    </Grid>
      )
    }
  }
  export default withStyles(styles)(LoginLanCenter)